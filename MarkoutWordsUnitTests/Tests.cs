﻿using System;
using MarkOutLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MarkoutWordsUnitTests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void SingleWordTest()
        {
            //Arrange
            var words = new string[] { "PHP" };
            var text = "We love coding in PHP!\nThis makes us really productive";
            var resultExpected = "We love coding in ***!\nThis makes us really productive";

            //Act
            var result = MarkOut.maskOutWords(words, text);

            //Assert
            Assert.AreEqual(resultExpected, result);
        }

        [TestMethod]
        public void MultipleWordsTest()
        {
            //Arange
            var words = new string[] { "yesteday", "Dog", "food", "walk" };
            var text = "Yesteday, I took my dog for a walk.\n It was crazy! My dog wanted only food.";
            var resultExpected = "********, I took my *** for a ****.\n It was crazy! My *** wanted only ****.";

            //Act
            var result = MarkOut.maskOutWords(words, text);

            //Assert
            Assert.AreEqual(resultExpected, result);
        }


        [TestMethod]
        public void EmpltyTextTest()
        {
            //Arange
            var words = new string[] { "aa", "bbb", "c", "dd" };
            var text = string.Empty;
            var resultExpected = string.Empty;

            //Act
            var result = MarkOut.maskOutWords(words, text);

            //Assert
            Assert.AreEqual(resultExpected, result);
        }

        [TestMethod]
        public void LongerWordTest()
        {
            //Arange
            var words = new string[] { "Home", "work", "test", "bag" };
            var text = "I have to finish my Homework, cuz I have a test!";
            var resultExpected = "I have to finish my Homework, cuz I have a ****!";

            //Act
            var result = MarkOut.maskOutWords(words, text);

            //Assert
            Assert.AreEqual(resultExpected, result);
        }

        [TestMethod]
        public void CaseSensitivityTest()
        {
            //Arange
            var words = new string[] { "WORK", "home", "test", "bag" };
            var text = "To work or to go HOME";
            var resultExpected = "To **** or to go ****";

            //Act
            var result = MarkOut.maskOutWords(words, text);

            //Assert
            Assert.AreEqual(resultExpected, result);
        }

        [TestMethod]
        public void HoleTextTest()
        {
            //Arange
            var words = new string[] { "test" };
            var text = "test";
            var resultExpected = "****";

            //Act
            var result = MarkOut.maskOutWords(words, text);

            //Assert
            Assert.AreEqual(resultExpected, result);
        }

        [TestMethod]
        public void MultipleTimesOfWordTest()
        {
            //Arange
            var words = new string[] { "test", "t" };
            var text = "test test testtest,TEST";
            var resultExpected = "**** **** testtest,****";

            //Act
            var result = MarkOut.maskOutWords(words, text);

            //Assert
            Assert.AreEqual(resultExpected, result);
        }
    }
}
