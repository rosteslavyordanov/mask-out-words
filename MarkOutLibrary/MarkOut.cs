﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkOutLibrary
{
    public static class MarkOut
    {
        /// <summary>
        /// Words that are selected as separator of words.
        /// It's free to be added other ones.
        /// </summary>
        private static char[] wordBreakers = new char[] { ' ', ',', '.', '!', '?', '\n', '\t' };

        // Problems: Only this ".ToLower()" is slow as shown in the first test,
        // when a lot of characters are to be lowered.
        // It take about 10-20 ms to pass.
        public static string maskOutWords(IEnumerable<string> words, string text)
        {
            if (string.IsNullOrWhiteSpace(text) ||
                words.Count() == 0)
            {
                return text;
            }

            string textToLower = text.ToLower();
            IEnumerable<string> wordsToLower = words.Select(word => word.ToLower());

            foreach (var word in wordsToLower)
            {
                MaskWord(ref text, ref textToLower, word);
            }

            return text;
        }

        /// <summary>
        /// Mask the word within a text,
        /// replacing it with its length of stars (*).
        /// </summary>
        /// <param name="text">The text in which this word in replaced.</param>
        /// <param name="textToLower">The second text in which this word in replaced.</param>
        /// <param name="word">The word to be replaced.</param>
        //private static void MaskWord(ref string text, ref string textToLower, string word)
        private static void MaskWord(ref string text, ref string textToLower, string word)
        {
            int startIndex = 0;
            CultureInfo info = CultureInfo.InvariantCulture;
            while (textToLower.IndexOf(word, startIndex) >= 0)
            {
                int index = textToLower.IndexOf(word, startIndex);

                // Checks whether the word in the beginning or in the end of text,
                // or it is separated for other words
                if ((index == 0 || IsSeparator(text[index - 1])) &&
                    (index + word.Length == textToLower.Length || IsSeparator(text[index + word.Length])))
                {
                    text = text.Substring(0, index)
                        + new string('*', word.Length)
                        + text.Substring(index + word.Length);
                    textToLower = textToLower.Substring(0, index)
                        + new string('*', word.Length)
                        + textToLower.Substring(index + word.Length); ;
                }
                startIndex = index + 1;
            }
        }

        /// <summary>
        /// Checks whether the character is a separator.
        /// It is in a separated method due to complexity.
        /// It improves readability.
        /// </summary>
        /// <param name="character">The character to be checked</param>
        /// <returns>True if it is a word separator. Other Ways is False</returns>
        private static bool IsSeparator(char character)
        {
            return wordBreakers.Any(ch => ch == character);
        }
    }
}
